# sway-dotfiles

![](assets/selenized-dark.png)

## Introduction
Dotfiles for my older Toshiba laptop.  Meant for personal use but if anything useful then please feel free to use. Uses, where I've been able, [Base16](https://github.com/chriskempson/base16) color theme, which seems to make it easier to swap between themes.  Using [Selenized](https://github.com/jan-warchol/selenized/blob/master/the-values.md) which is a variation of [Solarized](https://ethanschoonover.com/solarized/).

Assumes that Sway has been installed on Arch.  I've sddm as the display manager.  

Uses **Stow** to create the symlinks (see below).

## Downloading the dotfiles

In a terminal:

```bash
git clone https://gitlab.com/dajhub/sway-dotfiles
```
## Packages Installed

### Arch
Used [paru](https://github.com/Morganamilo/paru) (package manager).  To install Paru:

```bash
sudo pacman -S --needed base-devel && git clone https://aur.archlinux.org/paru.git  && cd paru && makepkg -si
```

Packages to install after installing paru can be found in the `scripts` folder.

### Fedora

Packages can be found in the `scripts` folder.  Packages may be slightly different from Arch since I am favouring the **Nemo** file manager over **Thunar**.

#### (i) Installing Tofi (Fedora)
See https://github.com/philj56/tofi

```bash
# Runtime dependencies
sudo dnf install freetype-devel cairo-devel pango-devel wayland-devel libxkbcommon-devel harfbuzz
# Build-time dependencies
sudo dnf install meson scdoc wayland-protocols-devel
```
Then build:
```bash
meson build && ninja -C build install
```


## Creating the symlinks

```bash
cd sway-dotfiles
stow .
```

## Printer Setup

```bash
yay -S --noconfirm print-manager system-config-printer hplip cups 
sudo systemctl start cups.service
sudo systemctl enable cups.service
```

## Set fish as default shell

```bash
chsh -s /usr/bin/fish
```

## Connect to Wi-Fi

In a terminal to identify Wi-Fi:

```bash
nmcli dev wifi
```

To connect (c) to identified Wi-Fi:

```bash
nmcli device wifi c <<SSID>> password <<PASSWORD>>
```

