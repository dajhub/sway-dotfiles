#!/bin/sh

# ---------------------------------------------
#                 INSTALLATION
# ---------------------------------------------

cd ~/

### Sway packages
sudo apt install -y sway waybar swaybg swayidle swaylock xwayland
sudo apt install -y gvfs-backends


### File manager (thunar)
sudo apt install -y thunar thunar-archive-plugin thunar-volman file-roller


### Terminal & editor
sudo apt install -y kitty micro fish


### Browser
sudo apt install -y chromium


### Power management
#sudo apt install -y mtools dosfstools acpi acpid avahi-daemon
#sudo systemctl enable acpid
#sudo systemctl enable avahi-daemon


### Autotiling: https://github.com/nwg-piotr/autotiling
cd
sudo apt install python3-i3ipc # install dependency
git clone https://github.com/nwg-piotr/autotiling.git
cd autotiling/autotiling
cp main.py ~/autotiling/autotiling/autotiling
chmod u+x autotiling
sudo mv autotiling /bin/
cd
rm -rf autotiling


### Fonts
sudo apt install -y fonts-inter fonts-jetbrains-mono fonts-material-design-icons-iconfont


### Screen recording
sudo apt install -y wf-recorder


### Logout menu
sudo apt install -y wlogout


### Polkit
sudo apt install -y polkit-kde-agent-1


### Network Manager
sudo apt install -y network-manager network-manager-gnome 


### Volume and brightness utilities
sudo apt install -y light pamixer


### Audio (pipewire)
sudo apt install -y pipewire-pulse pipewire-alsa pipewire-jack pipewire-audio libspa-0.2-bluetooth wireplumber pavucontrol


### Launcher and notification daemon
sudo apt install -y rofi wofi fuzzel sway-notification-center wob libnotify-bin


### Clipboard manager
sudo apt install -y  clipman


### Redshift replacement for wayland
sudo apt install -y wlsunset


### Screenshot
sudo apt install -y grim grimshot slurp imagemagick zenity wl-clipboard


### Build wl-color-picker
[ ! -d "~/.stuff/" ] && mkdir -p ~/.stuff && cd ~/.stuff
git clone https://github.com/jgmdev/wl-color-picker
cd wl-color-picker
sudo make install
cd


### QEMU virtualisation
sudo apt install -y virt-manager qemu-utils qemu-system-x86 qemu-utils libvirt-daemon-system


### Printer setup
sudo apt install -y system-config-printer hplip cups
sudo systemctl start cups.service
sudo systemctl enable cups.service


### Install code - source https://code.visualstudio.com/docs/setup/linux
sudo apt-get install -y gpg
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo install -D -o root -g root -m 644 packages.microsoft.gpg /etc/apt/keyrings/packages.microsoft.gpg
sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
rm -f packages.microsoft.gpg

sudo apt install -y apt-transport-https
sudo apt update
sudo apt install -y code

### Install OnlyOffice - source https://helpcenter.onlyoffice.com/installation/desktop-install-ubuntu.aspx

mkdir -p -m 700 ~/.gnupg
gpg --no-default-keyring --keyring gnupg-ring:/tmp/onlyoffice.gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys CB2DE8E5
chmod 644 /tmp/onlyoffice.gpg
sudo chown root:root /tmp/onlyoffice.gpg
sudo mv /tmp/onlyoffice.gpg /usr/share/keyrings/onlyoffice.gpg

echo 'deb [signed-by=/usr/share/keyrings/onlyoffice.gpg] https://download.onlyoffice.com/repo/debian squeeze main' | sudo tee -a /etc/apt/sources.list.d/onlyoffice.list

sudo apt-get update

sudo apt-get install -y onlyoffice-desktopeditors


### Installing Tofi

# Runtime dependencies
sudo apt install -y libfreetype-dev libcairo2-dev libpango1.0-dev libwayland-dev libxkbcommon-dev libharfbuzz-dev

# Build-time dependencies
sudo apt install -y meson scdoc wayland-protocols

# Download
git clone https://github.com/philj56/tofi

# Build
cd tofi
meson build && ninja -C build install
cd

### Install slick-greeter - display manager

sudo apt install -y lightdm slick-greeter

# Enable service
systemctl enable lightdm


### Change foot (terminal) theme
git clone https://github.com/catppuccin/foot
cd foot
cd themes
sudo mkdir -p /usr/share/foot/themes
sudo cp catppuccin-latte.ini /usr/share/foot/themes/catppuccin-latte.ini
cd

### Install nala - can be used instead of nala
sudo apt install -y nala

### Identifying bugs and important changes when installing packages or upgrades
sudo apt update && sudo apt install -y apt-listbugs apt-listchanges

### Set fish as default shell
chsh -s /usr/bin/fish

### Used for creating symlinks
sudo apt install stow


# ---------------------------------------------
#                CONFIGURATION
# ---------------------------------------------

cd ~/sway-dotfiles
stow .

### Wallpaper copied across
cd
mkdir -p ~/Pictures/Wallpapers
cp ~/sway-dotfiles/Wallpapers/CopenhagenBicycles.jpg ~/Pictures/Wallpapers/CopenhagenBicycles.jpg

