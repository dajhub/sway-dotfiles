if status is-interactive
    # Commands to run in interactive sessions can go here
end

export "MICRO_TRUECOLOR=1"

# oh-my-fish settings - using bobthefish theme: https://github.com/oh-my-fish/oh-my-fish/blob/master/docs/Themes.md#bobthefish
set -g theme_color_scheme light
set -g theme_display_date no
set -g fish_prompt_pwd_dir_length 0
function fish_greeting
  set_color $fish_color_autosuggestion
  echo "Welcome..."
  set_color normal
end