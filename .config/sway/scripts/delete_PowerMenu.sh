#!/bin/bash

SELECTION="$(printf "󰌾 Lock\n󰍃 Log out\n Reboot\n󰐥 Shutdown" | fuzzel --dmenu -a center -l 4 -w 18 -p "Select: ")"

#confirm_action() {
#    local action="$1"
#    CONFIRMATION="$(printf "No\nYes" | fuzzel --dmenu -a top-right -l 2 -w 18 -p "$action?")"
#    [[ "$CONFIRMATION" == *"Yes"* ]]
#}

case $SELECTION in
    *"󰌾 Lock"*)
        swaylock;;
    *"󰍃 Log out"*)
        swaymsg exit;;
    *" Reboot"*)
        systemctl reboot;;
    *"󰐥 Shutdown"*)
        systemctl poweroff;;
esac
