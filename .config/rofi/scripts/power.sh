
#!/usr/bin/env bash

# ───────────────────────────────────────────────────────────────
#                      Theme Settings
# ───────────────────────────────────────────────────────────────
dir="$HOME/.config/rofi/settings/powermenu/"

# ───────────────────────────────────────────────────────────────
#                         Menu Options
# ───────────────────────────────────────────────────────────────
shutdown=''
reboot=''
lock=''
logout=''

# ───────────────────────────────────────────────────────────────
#                           User Info
# ───────────────────────────────────────────────────────────────
username=" $(whoami)"
messages=("Powering down... but not for long!" "Powering down...")

# Pick a random message
sendoff="${messages[$((RANDOM % ${#messages[@]}))]}"

# ───────────────────────────────────────────────────────────────
#                           Rofi Cmd
# ───────────────────────────────────────────────────────────────
rofi_cmd() {
    rofi -dmenu \
        -p "$username" \
        -mesg "$sendoff" \
        -theme ${dir}/SelenizedDark.rasi
}

# ───────────────────────────────────────────────────────────────
#                       Run Rofi Menu
# ───────────────────────────────────────────────────────────────
run_rofi() {
    echo -e "$lock\n$logout\n$reboot\n$shutdown" | rofi_cmd
}

# ───────────────────────────────────────────────────────────────
#                        Command Exec
# ───────────────────────────────────────────────────────────────
run_cmd() {
    if [[ $1 == '--shutdown' ]]; then
        loginctl poweroff
    elif [[ $1 == '--reboot' ]]; then
        loginctl reboot
    elif [[ $1 == '--logout' ]]; then
        swaymsg exit
    fi
}

# ───────────────────────────────────────────────────────────────
#                           Execute Cmd
# ───────────────────────────────────────────────────────────────
chosen="$(run_rofi)"
case ${chosen} in
    $shutdown)
        loginctl poweroff
        ;;
    $reboot)
        loginctl reboot
        ;;
    $lock)
        swaylock
        ;;
    $logout)
        swaymsg exit
        ;;
esac

