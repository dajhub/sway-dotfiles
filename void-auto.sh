#!/bin/sh
# Void Linux Post-Installation Script for Sway [Wayland]


# Add multilib and nonfree repositories...
sudo xbps-install -Sy void-repo-nonfree void-repo-multilib void-repo-multilib-nonfree

# Update package lists and upgrade existing packages...
sudo xbps-install -Syu

# Install GPU drivers for Intel...
sudo xbps-install -Sy mesa-dri intel-video-accel vulkan-loader mesa-vulkan-intel

# Install recommended packages...
sudo xbps-install -Sy curl wget unzip zip gptfdisk mtools mlocate ntfs-3g fuse-exfat bash-completion linux-headers

# Install development tools...
sudo xbps-install -Sy autoconf automake bison m4 make libtool flex meson ninja optipng sassc

# Install sway...
sudo xbps-install -Sy wayland xorg elogind dbus-elogind seatd dunst sway swaybg swayidle swaylock autotiling light azote rofi tofi grimshot Waybar gvfs gvfs-mtp gvfs-gphoto2 htop wofi xf86-video-qxl mesa-dri ffmpeg
sudo xbps-install -Sy xdg-utils xdg-desktop-portal-wlr xdg-desktop-portal

# Install fish shell...
sudo xbps-install -Sy fish-shell
sudo usermod --shell /bin/fish "$USER"

# Install sddm (display manager)...
sudo xbps-install -Sy sddm

# Install terminals...
sudo xbps-install -Sy alacritty foot

# Install stow for symlinks
sudo xbps-install -Sy stow

# Install micro (editor)...
sudo xbps-install -Sy micro

# Install image viewer...
sudo xbps-install -Sy

# Install graphical text editor...
sudo xbps-install -Sy vscode

# Install browser...
sudo xbps-install -Sy vivaldi

# Install file manager...
sudo xbps-install -Sy Thunar thunar-archive-plugin xarchiver unzip p7zip unrar xz ffmpegthumbnailer tumbler

# Install NetworkManager, applet and enable service...
sudo xbps-install -Sy NetworkManager network-manager-applet
sudo ln -sv /etc/sv/NetworkManager /var/service

# Install fonts...
sudo xbps-install -Sy liberation-fonts-ttf dejavu-fonts-ttf ttf-ubuntu-font-family fonts-roboto-ttf noto-fonts-cjk-sans noto-fonts-ttf nerd-fonts-symbols-ttf

## Install flatpak packages
#sudo xbps-install -y flatpak
#flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
#flatpak install flathub io.gitlab.librewolf-community
#flatpak install flathub com.github.tchx84.Flatseal

# Set up PipeWire
sudo xbps-install -Sy pipewire wireplumber alsa-pipewire
#sudo ln -s /usr/share/applications/pipewire.desktop /etc/xdg/autostart/pipewire.desktop
sudo mkdir -p /etc/alsa/conf.d
sudo ln -s /usr/share/alsa/alsa.conf.d/50-pipewire.conf /etc/alsa/conf.d
sudo ln -s /usr/share/alsa/alsa.conf.d/99-pipewire-default.conf /etc/alsa/conf.d

# Set up dotfiles
cd sway-dotfiles
stow .
cd

# ENABLING SERVICES
## Enable dbus...
sudo xbps-install -Sy dbus
sudo ln -s /etc/sv/dbus /var/service
## Enable elogind...
sudo ln -s /etc/sv/elogind /var/service
## Enable polkit...
sudo xbps-install -Sy polkit
sudo ln -s /etc/sv/polkitd /var/service
## Install sddm (display manager) and enable...
sudo xbps-install -Sy sddm
sudo ln -sv /etc/sv/sddm /var/service

sudo reboot

######## Consider deleting below here


## Install other packages
#sudo xbps-install -S git tmux wayland dbus dbus-glib curl elogind polkit polkit-gnome chrony \
#             xdg-utils xdg-desktop-portal-gtk xdg-desktop-portal-wlr xdg-desktop-portal \
#             pipewire gstreamer1-pipewire libspa-bluetooth pavucontrol wlr-randr \
#             noto-fonts-emoji noto-fonts-cjk-sans noto-fonts-ttf nerd-fonts-symbols-ttf \
#             grim slurp wl-clipboard cliphist \
#             sway swayimg swaybg mpv mpvpaper ffmpeg yt-dlp \
#             fnott libnotify \
#             nnn unzip p7zip unrar xz pcmanfm-qt ffmpegthumbnailer lxqt-archiver gvfs-smb gvfs-afc gvfs-mtp udisks2 \
#             flavours breeze-gtk breeze-snow-cursor-theme breeze-icons \
#             qt5-wayland bluez \
#             labwc neovim foot Waybar wlsunset fuzzel brightnessctl bash-completion
#
#
#install_networking_packages() {
#  for pkg in fuse-sshfs lynx rsync wireguard; do
#    sudo xbps-install -y "$pkg"
#  done
#}
#
#install_flatpak_packages() {
#  sudo xbps-install -y flatpak
#  flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
#  flatpak install flathub io.gitlab.librewolf-community
#  flatpak install flathub com.github.tchx84.Flatseal
#}
#
#
#
## Set up PipeWire
#sudo ln -s /usr/share/applications/pipewire.desktop /etc/xdg/autostart/pipewire.desktop
#sudo mkdir -p /etc/alsa/conf.d
#sudo ln -s /usr/share/alsa/alsa.conf.d/50-pipewire.conf /etc/alsa/conf.d
#sudo ln -s /usr/share/alsa/alsa.conf.d/99-pipewire-default.conf /etc/alsa/conf.d
#
## Set up bluetooth autostart
#sudo ln -s /etc/sv/bluetoothd /var/service/
#
## Set up chrony
#sudo ln -s /etc/sv/chronyd /var/service/
#
## Set up polkitd
#sudo ln -s /etc/sv/polkitd /var/service/
#
## Remove unused services (TTYs)
#for tty in 3 4 5 6; do
#  sudo rm -rf /var/service/agetty-tty"$tty"
#done
#
## Set up ACPI
#sudo ln -s /etc/sv/acpid/ /var/service/
#sudo sv enable acpid
#sudo sv start acpid
#
## Improve font rendering
#for conf in 11-lcdfilter-default.conf 10-sub-pixel-rgb.conf 10-hinting-slight.conf; do
#  sudo ln -s /etc/fonts/conf.avail/"$conf" /etc/fonts/conf.d
#done
#
## Set up NetworkManager
#sudo xbps-install -Sy NetworkManager dbus
#if sudo sv status wpa_supplicant >/dev/null 2>&1; then
#  sudo sv stop wpa_supplicant
#fi
#
#sudo rm -rf /var/services/wpa_supplicant 2>/dev/null
#sudo ln -s /etc/sv/dbus /var/service
#sudo ln -s /etc/sv/NetworkManager /var/service
#sudo sv start NetworkManager
#
## Clone and set up dotfiles
#git clone https://github.com/speyll/dotfiles "$HOME/dotfiles"
#cp -r "$HOME/dotfiles/."* "$HOME/"
#rm -rf "$HOME/dotfiles"
#
#chmod -R +X "$HOME/.local/bin" "$HOME/.local/share/applications" "$HOME/.config/autostart/" # Adjust permissions
#ln -s "$HOME/.config/mimeapps.list" "$HOME/.local/share/applications/" # Create symbolic link for mimeapps.list
#dash "$HOME/.local/share/fonts/git-fonts.sh" # Run the font installation script
#
## Add user to wheel group for sudo access
#echo "%sudo ALL=(ALL:ALL) NOPASSWD: /usr/bin/halt, /usr/bin/poweroff, /usr/bin/reboot, /usr/bin/shutdown, /usr/bin/zzz, /usr/bin/ZZZ" | sudo tee -a /etc/sudoers.d/wheel